from django import forms
from user.models import Profile
from .models import Project, TaskFile, TaskOffer, Delivery, ProjectCategory, Team
from django.contrib.auth.models import User


class ProjectForm(forms.ModelForm):
    title = forms.CharField(max_length=200)
    description = forms.Textarea()
    category_id = forms.ModelChoiceField(queryset=ProjectCategory.objects.all())
    thumbnail = forms.FileField(required=False, label="Thumbnail (Optional)")

    class Meta:
        model = Project
        fields = ('title', 'description', 'category_id', 'thumbnail')

    def clean_thumbnail(self):
        file = self.cleaned_data['thumbnail']
        if file is not None:
            accepted_extensions = (".png", ".jpeg", ".jpg")
            if not file.name.lower().endswith(accepted_extensions):
                raise forms.ValidationError("Thumbnail must be png, jpeg, or jpg file")
        return file



class CategoryForm(forms.ModelForm):
    name = forms.CharField(max_length=200)

    class Meta:
        model = ProjectCategory
        fields = ('name',)

    def clean_name(self):
        data = self.cleaned_data["name"]
        if ProjectCategory.objects.filter(name__iexact=data).__len__() > 0:
            raise forms.ValidationError("Category \""+data+"\" already exists")
        return data




class TaskFileForm(forms.ModelForm):
    file = forms.FileField()

    class Meta:
        model = TaskFile
        fields = ('file',)


class ProjectStatusForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('status',)


class TaskOfferForm(forms.ModelForm):
    title = forms.CharField(max_length=200)
    description = forms.Textarea()
    price = forms.NumberInput()

    class Meta:
        model = TaskOffer
        fields = ('title', 'description', 'price',)

    def clean_price(self):
        data = self.cleaned_data["price"]
        if data < 0:
            raise forms.ValidationError('Offer must not be negative')
        return data



class TaskOfferResponseForm(forms.ModelForm):
    feedback = forms.Textarea()

    class Meta:
        model = TaskOffer
        fields = ('status', 'feedback')


class TaskDeliveryResponseForm(forms.ModelForm):
    feedback = forms.Textarea()

    class Meta:
        model = Delivery
        fields = ('status', 'feedback')


PERMISSION_CHOICES = (
    ('Read', 'Read'),
    ('Write', 'Write'),
    ('Modify', 'Modify'),
)


class TaskPermissionForm(forms.Form):
    user = forms.ModelChoiceField(queryset=User.objects.all())
    permission = forms.ChoiceField(choices=PERMISSION_CHOICES)


class DeliveryForm(forms.ModelForm):
    comment = forms.Textarea()
    file = forms.FileField()

    class Meta:
        model = Delivery
        fields = ('comment', 'file')


class TeamForm(forms.ModelForm):
    name = forms.CharField(max_length=50)

    class Meta:
        model = Team
        fields = ('name',)


class TeamAddForm(forms.ModelForm):
    members = forms.ModelMultipleChoiceField(queryset=Profile.objects.all(), label='Members with read')

    class Meta:
        model = Team
        fields = ('members',)
