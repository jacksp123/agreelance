from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, Client
from django.urls import reverse
import random
import string

import projects
from projects.forms import ProjectStatusForm, TaskOfferForm
from projects.models import Project, ProjectCategory, Task, TaskOffer
from django.contrib.auth.models import User


# Create your tests here.
from projects.views import get_user_task_permissions


def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def create_project(user, status):
    category = ProjectCategory.objects.create(name="testProjectCategory")

    thumbnail = SimpleUploadedFile(user.username+status+
        randomString(10) + ".testFile",
        b"these are the file contents!"
    )

    project = Project.objects.create(user=user.profile, title="testProject", description="testDescricption",
                                     category=category, thumbnail=thumbnail, status=status)
    Task.objects.create(project=project, title="testTitle", description="testdesc")

    return project


class SystemTestsSecurity(TestCase):
    def setUp(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        user = User.objects.create(username='testadmin')
        user.set_password('12345')
        user.is_staff = True
        user.save()

        create_project(user=user, status="o")

    def tearDown(self):
        project = Project.objects.get(title="testProject")
        project.thumbnail.delete()

    def test_security_add_category_not_logged_in(self):

        client = Client()
        url = reverse(projects.views.new_category)

        category_exists = ProjectCategory.objects.filter(name__exact='testCategory').count() > 0
        self.assertFalse(category_exists)

        client.post(url, {'name': 'testCategory'}, follow=True)

        category_exists = ProjectCategory.objects.filter(name__exact='testCategory').count() > 0
        self.assertFalse(category_exists)

    def test_security_add_category_not_admin(self):

        client = Client()
        client.login(username='testuser', password='12345')
        url = reverse(projects.views.new_category)

        category_exists = ProjectCategory.objects.filter(name__exact='testCategory').count() > 0
        self.assertFalse(category_exists)

        client.post(url, {'name': 'testCategory'}, follow=True)

        category_exists = ProjectCategory.objects.filter(name__exact='testCategory').count() > 0
        self.assertFalse(category_exists)

    def test_security_delete_project_not_logged_in(self):
        project = Project.objects.get(title="testProject")

        client = Client()

        url = reverse(projects.views.delete_project, args=(project.id,))

        project_exists = Project.objects.filter(title__exact='testProject').count() > 0
        self.assertTrue(project_exists)

        client.post(url,  follow=True)

        project_exists = Project.objects.filter(title__exact='testProject').count() > 0
        self.assertTrue(project_exists)

    def test_security_delete_project_not_project_owner(self):
        project = Project.objects.get(title="testProject")

        user = User.objects.create(username='otherTestUser')
        user.set_password('12345')
        user.save()

        client = Client()
        client.login(username='otherTestUser', password='12345')

        url = reverse(projects.views.delete_project, args=(project.id,))

        project_exists = Project.objects.filter(title__exact='testProject').count() > 0
        self.assertTrue(project_exists)

        client.post(url,  follow=True)

        project_exists = Project.objects.filter(title__exact='testProject').count() > 0
        self.assertTrue(project_exists)

class SytemTestRobustness(TestCase):
    def setUp(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        user = User.objects.create(username='testadmin')
        user.set_password('12345')
        user.is_staff = True
        user.save()


    def test_special_value_add_existing_category(self):

        client = Client()
        client.login(username='testadmin', password='12345')

        url = reverse(projects.views.new_category)

        category_exists = ProjectCategory.objects.filter(name__exact='testCategory2').count() > 0
        self.assertFalse(category_exists)

        client.post(url, {'name': 'testCategory2'}, follow=True)

        category_exists = ProjectCategory.objects.filter(name__exact='testCategory2').count() > 0
        self.assertTrue(category_exists)

        client.post(url, {'name': 'testCategory2'}, follow=True)

        category_exists_once = ProjectCategory.objects.filter(name__exact='testCategory2').count() == 1
        self.assertTrue(category_exists_once)

    def test_special_value_thumbnail_wrong_file_format(self):
        category = ProjectCategory.objects.create(name="testCategory")

        client = Client()
        client.login(username='testuser', password='12345')

        url = reverse(projects.views.new_project)
        thumbnail = SimpleUploadedFile(
            randomString(10)+".badformat",
            b"these are the file contents!"
        )

        client.post(url, {'title':'testProject', 'description':'testDescription', 'category_id': category.id,
                                     'thumbnail': thumbnail}, follow=True)

        project_exists = Project.objects.filter(title__exact='testProject').count() > 0
        self.assertFalse(project_exists)


class DeleteProjectIntegrationTests(TestCase):
    def setUp(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        create_project(user=user, status="o")

    def test_project_is_deleted_from_database_on_delete(self):
        project = Project.objects.get(title="testProject")

        client = Client()
        client.login(username='testuser', password='12345')

        url = reverse(projects.views.delete_project, args=(project.id,))

        project_exists = Project.objects.filter(title__exact='testProject').count() > 0
        self.assertTrue(project_exists)

        client.post(url,  follow=True)

        project_exists = Project.objects.filter(title__exact='testProject').count() > 0
        self.assertFalse(project_exists)

    def test_redirect_to_projects_on_form_submit(self):
        project = Project.objects.get(title="testProject")

        client = Client()
        client.login(username='testuser', password='12345')

        url = reverse(projects.views.delete_project, args=(project.id,))
        expected_redirect_url = reverse(projects.views.projects)

        response = client.post(url, follow=True)
        self.assertRedirects(response=response, expected_url=expected_redirect_url)


class CreateProjectIntegrationTests(TestCase):
    def setUp(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.is_staff = True
        user.save()

    def test_category_creation_on_form_submit(self):

        client = Client()
        client.login(username='testuser', password='12345')

        url = reverse(projects.views.new_category)

        category_exists = ProjectCategory.objects.filter(name__exact='testCategory').count() > 0
        self.assertFalse(category_exists)

        client.post(url, {'name': 'testCategory'}, follow=True)

        category_exists = ProjectCategory.objects.filter(name__exact='testCategory').count() > 0
        self.assertTrue(category_exists)

    def test_redirect_to_projects_on_form_submit(self):

        client = Client()
        client.login(username='testuser', password='12345')

        url = reverse(projects.views.new_category)
        expected_redirect_url = reverse(projects.views.projects)

        response = client.post(url, {'name': 'testCategory'}, follow=True)
        self.assertRedirects(response=response, expected_url=expected_redirect_url)


class UploadThumbNailIntegrationTests(TestCase):
    def setUp(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()


    def test_thumbnail_is_created_on_project_creation(self):
        category = ProjectCategory.objects.create(name="testCategory")

        client = Client()
        client.login(username='testuser', password='12345')

        url = reverse(projects.views.new_project)
        thumbnail = SimpleUploadedFile(
            randomString(10)+".png",
            b"these are the file contents!"  # note the b in front of the string [bytes]
        )

        client.post(url, {'title':'testProject', 'description':'testDescription', 'category_id': category.id,
                                     'thumbnail': thumbnail}, follow=True)

        project = Project.objects.get(title='testProject')
        self.assertEqual(project.thumbnail.name, thumbnail.name)

        project = Project.objects.filter(title__exact='testProject')
        if project.count() > 0:
            project[0].thumbnail.delete()




class AcceptOfferTests(TestCase):
    def setUp(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        project_user = User.objects.create(username='projectuser')
        project_user.set_password('12345')
        project_user.save()

        project = create_project(user=project_user, status="o")

        task = Task.objects.create(title='testTask', description='taskDesc', budget=9, project=project)

        TaskOffer.objects.create(task=task, title='testTaskOffer', offerer=user.profile, feedback="testFeedback",
                                 status='p')

    def tearDown(self):
        project = Project.objects.get(title="testProject")
        project.thumbnail.delete()

    def test_accept_offer(self):
        task_offer = TaskOffer.objects.get(title='testTaskOffer')
        project = Project.objects.get(title="testProject")
        task = Task.objects.get(title="testTask")

        expected_feedback = 'helloHello'

        client = Client()
        client.login(username='projectuser', password='12345')

        url = reverse(projects.views.project_view, args=(project.id,))

        response = client.post(url,
                               {'offer_response': 'offer_response',
                                'taskofferid': task_offer.id, 'status': 'a',
                                'feedback': expected_feedback}, follow=True)

        offer_response = TaskOffer.objects.get(task=task)
        self.assertEqual(offer_response.status, 'a')
        self.assertEqual(offer_response.feedback, expected_feedback)


    def test_pending_offer(self):
        task_offer = TaskOffer.objects.get(title='testTaskOffer')
        project = Project.objects.get(title="testProject")
        task = Task.objects.get(title="testTask")

        expected_feedback = 'helloHello'

        client = Client()
        client.login(username='projectuser', password='12345')

        url = reverse(projects.views.project_view, args=(project.id,))

        response = client.post(url,
                               {'offer_response': 'offer_response',
                                'taskofferid': task_offer.id, 'status': 'p',
                                'feedback': expected_feedback}, follow=True)

        offer_response = TaskOffer.objects.get(task=task)
        self.assertEqual(offer_response.status, 'p')
        self.assertEqual(offer_response.feedback, expected_feedback)

    def test_decline_offer(self):
        task_offer = TaskOffer.objects.get(title='testTaskOffer')
        project = Project.objects.get(title="testProject")
        task = Task.objects.get(title="testTask")

        expected_feedback = 'helloHello'

        client = Client()
        client.login(username='projectuser', password='12345')

        url = reverse(projects.views.project_view, args=(project.id,))

        response = client.post(url,
                               {'offer_response': 'offer_response',
                                'taskofferid': task_offer.id, 'status': 'd',
                                'feedback': expected_feedback}, follow=True)

        offer_response = TaskOffer.objects.get(task=task)
        self.assertEqual(offer_response.status, 'd')
        self.assertEqual(offer_response.feedback, expected_feedback)


class task_offer_form_tests(TestCase):
    def test_valid_form_data(self):
        form_data = {'title':'testTitle', 'description':'testDescription', 'price': 99}
        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())
    def test_price_min_plus(self):
        form_data = {'title':'testTitle', 'description':'testDescription', 'price': 1}
        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())
    def test_price_min(self):
        form_data = {'title':'testTitle', 'description':'testDescription', 'price': 0}
        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())
    def test_price_min_minus(self):
        form_data = {'title':'testTitle', 'description':'testDescription', 'price': -1}
        form = TaskOfferForm(form_data)
        self.assertFalse(form.is_valid())
    def test_string_for_price_invalid(self):
        form_data = {'title':'testTitle', 'description':'testDescription', 'price': 'sdflkj'}
        form = TaskOfferForm(form_data)
        self.assertFalse(form.is_valid())


    def test_title_empty(self):
        form_data = {'title':'', 'description':'testDescription', 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertFalse(form.is_valid())
    def test_title_first_character_symbol(self):
        form_data = {'title':')testTitle', 'description':'testDescription', 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())
    def test_title_special_characters(self):
        form_data = {'title':'testØTitle', 'description':'testDescription', 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())
    def test_title_length_max_minus(self):
        form_data = {'title': randomString(199), 'description':'testDescription', 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())
    def test_title_length_max(self):
        form_data = {'title': randomString(200), 'description':'testDescription', 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())
    def test_title_length_max_plus(self):
        form_data = {'title': randomString(201), 'description':'testDescription', 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertFalse(form.is_valid())


    def test_description_empty(self):
        form_data = {'title':'testTitle', 'description':'', 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertFalse(form.is_valid())
    def test_description_first_character_symbol(self):
        form_data = {'title':'testTitle', 'description':'{testDescription', 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())
    def test_description_special_characters(self):
        form_data = {'title':'testTitle', 'description':'testØDescription', 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())
    def test_description_length_max_minus(self):
        form_data = {'title': 'testTitle', 'description': randomString(499), 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())
    def test_description_length_max(self):
        form_data = {'title': 'testTitle', 'description': randomString(500), 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertTrue(form.is_valid())
    def test_description_length_max_plus(self):
        form_data = {'title': 'testTitle', 'description': randomString(501), 'price': 9}
        form = TaskOfferForm(form_data)
        self.assertFalse(form.is_valid())



class get_user_task_permissions_tests(TestCase):

    def tearDown(self):
        project = Project.objects.get(title="testProject")
        project.thumbnail.delete()

    def test_user_is_task_owner(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        project = create_project(user, 'sdaflk;sjf')
        task = Task.objects.create(project=project)

        expected = {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        }
        result = get_user_task_permissions(user, task)

        self.assertEqual(result, expected)

    def test_user_is_task_offerer(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        other_user = User.objects.create(username='testuser2')
        other_user.set_password('12345')
        other_user.save()

        project = create_project(user, 'sdaflk;sjf')
        task = Task.objects.create(project=project)

        TaskOffer.objects.create(task=task, title='testTaskOffer', offerer=other_user.profile, feedback="testFeedback",
                                 status='a')
        expected = {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        }
        result = get_user_task_permissions(other_user, task)

        self.assertEqual(result, expected)

    def test_user_is_not_owner_or_offerer(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        other_user = User.objects.create(username='testuser2')
        other_user.set_password('12345')
        other_user.save()

        project = create_project(user, 'sdaflk;sjf')
        task = Task.objects.create(project=project)
        expected = {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False,
        }
        result = get_user_task_permissions(other_user, task)

        self.assertEqual(result, expected)


class project_view_tests(TestCase):

    def setUp(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        project_user = User.objects.create(username='projectuser')
        project_user.set_password('12345')
        project_user.save()

        project = create_project(user=project_user, status="o")

        task = Task.objects.create(title='testTask', description='taskDesc', budget=9, project=project)

        TaskOffer.objects.create(task=task, title='testTaskOffer', offerer=user.profile, feedback="testFeedback", status='a')

    def tearDown(self):
        project = Project.objects.get(title="testProject")
        project.thumbnail.delete()

    def test_user_not_project_owner(self):
        project = Project.objects.get(title="testProject")
        task = Task.objects.get(title="testTask")

        client = Client()
        client.login(username='testuser', password='12345')

        url = reverse(projects.views.project_view, args=(project.id,))

        response = client.post(url,
                               {'offer_submit': 'offer_submit', 'title': task.title, 'description': task.description,
                                'price': task.budget, 'taskvalue': task.id}, follow=True)
        self.assertEqual(response.context['project'].id, project.id)

    def test_user_project_owner_offer_response(self):

        task_offer = TaskOffer.objects.get(title='testTaskOffer')
        project = Project.objects.get(title="testProject")

        client = Client()
        client.login(username='projectuser', password='12345')

        url = reverse(projects.views.project_view, args=(project.id,))

        response = client.post(url,
                               {'offer_response': 'offer_response',
                                'taskofferid': task_offer.id, 'status': task_offer.status,
                                'feedback': task_offer.feedback}, follow=True)
        self.assertEqual(response.context['project'].id, project.id)

    def test_user_project_owner_status_change(self):

        project = Project.objects.get(title="testProject")

        client = Client()
        client.login(username='projectuser', password='12345')

        url = reverse(projects.views.project_view, args=(project.id,))

        response = client.post(url, {'status_change': 'status_change', 'status': project.status}, follow=True)
        self.assertEqual(response.context['project'].id, project.id)


