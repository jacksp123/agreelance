from unittest import skip

from django.test import TestCase
from projects.models import ProjectCategory

from projects.tests import randomString
from user.forms import SignUpForm


def build_form_data(username='test', first_name='test', last_name='test', company='test', email='test@test.com',
                    email_confirmation='test@test.com', password1='uliuyg565', password2='uliuyg565',
                    phone_number='test', country='test', state='test', city='test', postal_code='test',
                    street_address='test'):
    ProjectCategory.objects.create(name='testCategory')
    return {'username': username, 'first_name': first_name, 'last_name': last_name,
            'categories': ProjectCategory.objects.all(), 'company': company, 'email': email,
            'email_confirmation': email_confirmation, 'password1': password1, 'password2': password2,
            'phone_number': phone_number, 'country': country, 'state': state, 'city': city, 'postal_code': postal_code,
            'street_address': street_address}

class SignUpPagePairwiseDomainTests(TestCase):
    def setUp(self):
        ProjectCategory.objects.create(name='testCategory')

    def test_pairwise(self):
        #all data is good
        form_data = {'username': 'username', 'first_name': 'first_name', 'last_name': 'last_name',
                'categories': ProjectCategory.objects.all(), 'company': 'company', 'email': 'email1@test.com',
                'email_confirmation': 'email1@test.com', 'password1': 'goodpassword', 'password2': 'goodpassword',
                'phone_number': 'phone_number', 'country': 'Norway', 'state': 'trondelag', 'city': 'trondheim', 'postal_code': 7050,
                'street_address': 'some address'}
        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid(),msg=form.errors)

    def test_pairwise2(self):
        #same password check should fail
        #password similar to first_name should fail
        form_data = {'username': 'username', 'first_name': 'first_name', 'last_name': 'last_name',
                'categories': ProjectCategory.objects.all(), 'company': 'company', 'email': 'email1@test.com',
                'email_confirmation': 'email1@test.com', 'password1': 'first_name', 'password2': 'goodpassword',
                'phone_number': 'phone_number', 'country': 'USA', 'state': 'Wisconsin', 'city': 'Madison', 'postal_code': 53715,
                'street_address': 'some address'}
        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

    def test_pairwise3(self):
        #same password check should fail
        #password similar to last_name should fail
        #state in country check should fail
        #postal code in city should fail
        #email confirmation should fail
        form_data = {'username': 'username', 'first_name': 'first_name', 'last_name': 'last_name',
                'categories': ProjectCategory.objects.all(), 'company': '', 'email': 'email1@test.com',
                'email_confirmation': 'email2@test.com', 'password1': 'last_name', 'password2': 'goodpassword',
                'phone_number': 'phone_number', 'country': 'USA', 'state': 'trondelag', 'city': 'Madison', 'postal_code': 7050,
                'street_address': 'some address'}
        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

    def test_pairwise4(self):
        #same password check should fail
        #password similar to username should fail
        #state in country check should fail
        #city in state should fail
        #email confirmation should fail
        form_data = {'username': 'username', 'first_name': 'first_name', 'last_name': 'last_name',
                'categories': ProjectCategory.objects.all(), 'company': 'company', 'email': 'email1@test.com',
                'email_confirmation': 'email1@test.com', 'password1': 'username', 'password2': 'goodpassword',
                'phone_number': 'phone_number', 'country': 'Norway', 'state': 'Wisconsin', 'city': 'trondheim', 'postal_code': 53715,
                'street_address': 'some address'}
        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

    @skip("functionality to check location isn't implemented")
    def test_pairwise5(self):
        #city in state should fail
        form_data = {'username': 'username', 'first_name': 'first_name', 'last_name': 'last_name',
                'categories': ProjectCategory.objects.all(), 'company': 'company', 'email': 'email1@test.com',
                'email_confirmation': 'email1@test.com', 'password1': 'goodpassword', 'password2': 'goodpassword',
                'phone_number': 'phone_number', 'country': 'USA', 'state': 'Wisconsin', 'city': 'trondheim', 'postal_code': 7050,
                'street_address': 'some address'}
        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

    @skip("functionality to check location isn't implemented")
    def test_pairwise6(self):
        #postal code in city should fail
        form_data = {'username': 'username', 'first_name': 'first_name', 'last_name': 'last_name',
                'categories': ProjectCategory.objects.all(), 'company': 'company', 'email': 'email1@test.com',
                'email_confirmation': 'email1@test.com', 'password1': 'goodpassword', 'password2': 'goodpassword',
                'phone_number': 'phone_number', 'country': 'Norway', 'state': 'trondelag', 'city': 'trondheim', 'postal_code': 53715,
                'street_address': 'some address'}
        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

class SignUpPageTests(TestCase):

    def test_all_valid_data(self):
        form = SignUpForm(build_form_data())
        self.assertTrue(form.is_valid())

    def test_first_name(self):
        self.run_all_boundry_tests_on_string("first_name", 30)

    def test_username(self):
        self.empty_string("username")
        self.first_character_symbol("username", symbols_allowed=False)
        self.special_characters("username")
        self.run_string_max_length_tests("username", 150)

    def test_last_name(self):
        self.run_all_boundry_tests_on_string("last_name", 30)

    def test_company(self):
        self.empty_string("company", required=False)
        self.first_character_symbol("company")
        self.special_characters("company")
        self.run_string_max_length_tests("company", 30)

    def test_email(self):
        self.empty_string("email")
        self.first_character_symbol("email", test_string='*dkjf@test.com')
        self.special_characters("email", test_string='ØtØestØ@test.com', special_characters_allowed=False)
        self.run_string_max_length_tests("email", 254, email=True)

    def test_email_confirmation(self):
        self.empty_string("email_confirmation")
        self.first_character_symbol("email_confirmation", test_string='*dkjf@test.com')
        self.special_characters("email_confirmation", test_string='ØtØestØ@test.com', special_characters_allowed=False)
        self.run_string_max_length_tests("email_confirmation", 254, email=True)

    # password1 and password2 are tested together because the must match or the form will always fail
    def test_all_password_boundries(self):
        self.empty_string_password()
        self.first_character_symbol_password()
        self.special_characters_password()
        self.length_min_minus_password()
        self.length_min_password()
        self.length_min_minus_password()

    def test_phone_number(self):
        self.run_all_boundry_tests_on_string("phone_number", 50)

    def test_country(self):
        self.run_all_boundry_tests_on_string("country", 50)

    def test_state(self):
        self.run_all_boundry_tests_on_string("state", 50)

    def test_city(self):
        self.run_all_boundry_tests_on_string("city", 50)

    def test_postal_code(self):
        self.run_all_boundry_tests_on_string("postal_code", 50)

    def test_street_address(self):
        self.run_all_boundry_tests_on_string("street_address", 50)

    def run_all_boundry_tests_on_string(self, variable_to_test_name, maximum_size):
        self.run_character_boundry_tests(variable_to_test_name)
        self.run_string_max_length_tests(variable_to_test_name, maximum_size)

    def run_character_boundry_tests(self, variable_to_test_name):
        self.empty_string(variable_to_test_name)
        self.first_character_symbol(variable_to_test_name)
        self.special_characters(variable_to_test_name)

    def run_string_max_length_tests(self, variable_to_test_name, maximum_size, email=False):
        self.length_max_minus(variable_to_test_name, maximum_size, email=email)
        self.length_max(variable_to_test_name, maximum_size, email=email)
        self.length_max_plus(variable_to_test_name, maximum_size, email=email)

    def empty_string(self, variable_to_test, required=True):
        form_data = build_form_data()
        form_data[variable_to_test] = ''
        form = SignUpForm(form_data)
        if required:
            self.assertFalse(form.is_valid())
        else:
            self.assertTrue(form.is_valid())

    def first_character_symbol(self, variable_to_test, test_string='(asdfkj', symbols_allowed=True):
        form_data = build_form_data()
        form_data[variable_to_test] = test_string
        form = SignUpForm(form_data)
        self.assertEqual(symbols_allowed, form.is_valid())

    def special_characters(self, variable_to_test, test_string='testØ', special_characters_allowed=True):
        form_data = build_form_data()
        form_data[variable_to_test] = test_string
        form = SignUpForm(form_data)
        self.assertEqual(special_characters_allowed, form.is_valid())

    def length_max_minus(self, variable_to_test, maximum_size, email=False):
        form_data = build_form_data()
        adjusted_size = maximum_size - 9 if email else maximum_size
        form_data[variable_to_test] = randomString(adjusted_size - 1)
        if email:
            form_data[variable_to_test] += '@test.com'
        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def length_max(self, variable_to_test, maximum_size, email=False):
        form_data = build_form_data()
        adjusted_size = maximum_size - 9 if email else maximum_size
        form_data[variable_to_test] = randomString(adjusted_size)
        if email:
            form_data[variable_to_test] += '@test.com'
        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def length_max_plus(self, variable_to_test, maximum_size, email=False):
        form_data = build_form_data()
        adjusted_size = maximum_size - 9 if email else maximum_size
        form_data[variable_to_test] = randomString(adjusted_size + 1)
        if email:
            form_data[variable_to_test] += '@test.com'
        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

    def empty_string_password(self):
        form_data = build_form_data(password1='', password2='')
        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

    def first_character_symbol_password(self):
        form_data = build_form_data(password1='(sdlkfj4lw3h', password2='(sdlkfj4lw3h')
        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def special_characters_password(self):
        form_data = build_form_data(password1='(sdØlkfjØ4lw3h', password2='(sdØlkfjØ4lw3h')
        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def length_min_plus_password(self):
        password = randomString(9)
        form_data = build_form_data(password1=password, password2=password)
        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def length_min_password(self):
        password = randomString(8)
        form_data = build_form_data(password1=password, password2=password)
        form = SignUpForm(form_data)
        self.assertTrue(form.is_valid())

    def length_min_minus_password(self):
        password = randomString(7)
        form_data = build_form_data(password1=password, password2=password)
        form = SignUpForm(form_data)
        self.assertFalse(form.is_valid())

# Create your tests here.
